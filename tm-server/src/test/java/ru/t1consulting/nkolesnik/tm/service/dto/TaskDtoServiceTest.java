package ru.t1consulting.nkolesnik.tm.service.dto;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.t1consulting.nkolesnik.tm.api.service.IConnectionService;
import ru.t1consulting.nkolesnik.tm.api.service.IPropertyService;
import ru.t1consulting.nkolesnik.tm.api.service.dto.IProjectDtoService;
import ru.t1consulting.nkolesnik.tm.api.service.dto.ITaskDtoService;
import ru.t1consulting.nkolesnik.tm.dto.model.ProjectDTO;
import ru.t1consulting.nkolesnik.tm.dto.model.TaskDTO;
import ru.t1consulting.nkolesnik.tm.enumerated.Status;
import ru.t1consulting.nkolesnik.tm.exception.entity.StatusNotFoundException;
import ru.t1consulting.nkolesnik.tm.exception.entity.TaskNotFoundException;
import ru.t1consulting.nkolesnik.tm.exception.entity.UserNotFoundException;
import ru.t1consulting.nkolesnik.tm.exception.field.DescriptionEmptyException;
import ru.t1consulting.nkolesnik.tm.exception.field.NameEmptyException;
import ru.t1consulting.nkolesnik.tm.exception.field.TaskIdEmptyException;
import ru.t1consulting.nkolesnik.tm.exception.field.UserIdEmptyException;
import ru.t1consulting.nkolesnik.tm.marker.DataCategory;
import ru.t1consulting.nkolesnik.tm.service.ConnectionService;
import ru.t1consulting.nkolesnik.tm.service.PropertyService;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Category(DataCategory.class)
public class TaskDtoServiceTest {

    @NotNull
    private static final String TASK_NAME_PREFIX = "TEST_TASK_NAME";

    @NotNull
    private static final String TASK_DESCRIPTION_PREFIX = "TEST_TASK_DESCRIPTION";

    @Nullable
    private static final String NULL_TASK_NAME = null;

    @Nullable
    private static final String NULL_TASK_DESCRIPTION = null;

    @Nullable
    private static final Status NULL_STATUS = null;

    @Nullable
    private static final String NULL_TASK_ID = null;

    @Nullable
    private static final Integer NULL_TASK_INDEX = null;

    @Nullable
    private static final String NULL_USER_ID = null;

    @NotNull
    private static final String EMPTY_TASK_ID = "";

    @NotNull
    private static final String EMPTY_USER_ID = "";

    @Nullable
    private static final TaskDTO NULL_TASK = null;

    private static final long REPOSITORY_SIZE = 10L;

    private static final long EMPTY_REPOSITORY_SIZE = 0L;

    @NotNull
    private static final IPropertyService propertyService = new PropertyService();

    @NotNull
    private static final IConnectionService connectionService = new ConnectionService(propertyService);
    @NotNull
    private static final IProjectDtoService projectService = new ProjectDtoService(connectionService);
    @NotNull
    private static String userId = "";
    @NotNull
    private static String projectId = UUID.randomUUID().toString();
    @NotNull
    private final ITaskDtoService taskService = new TaskDtoService(connectionService);
    @NotNull
    private final String taskId = UUID.randomUUID().toString();

    @NotNull
    private List<TaskDTO> tasks;

    @NotNull
    private TaskDTO task;

    @BeforeClass
    public static void prepareTestEnvironment() {
        getTestData();
        saveDbBackup();
    }

    @AfterClass
    public static void restoreProdEnvironment() {
        loadDbBackup();
    }

    @SneakyThrows
    public static void saveDbBackup() {
        @NotNull final String userName = propertyService.getDatabaseUsername();
        @NotNull final String userPassword = propertyService.getDatabasePassword();
        @NotNull final String url = propertyService.getDatabaseConnectionString();
        @NotNull final Connection connection = DriverManager.getConnection(url, userName, userPassword);
        connection.setAutoCommit(false);
        String sqlSaveData = "SELECT * INTO \"backup_tasks\" FROM \"tasks\";";
        String sqlClearTable = "DELETE FROM \"tasks\";";
        try {
            Statement statement = connection.createStatement();
            statement.executeUpdate(sqlSaveData);
            statement.executeUpdate(sqlClearTable);
            connection.commit();
        } catch (Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @SneakyThrows
    public static void loadDbBackup() {
        @NotNull final String userName = propertyService.getDatabaseUsername();
        @NotNull final String userPassword = propertyService.getDatabasePassword();
        @NotNull final String url = propertyService.getDatabaseConnectionString();
        @NotNull final Connection connection = DriverManager.getConnection(url, userName, userPassword);
        connection.setAutoCommit(false);
        String sqlClearTable = "DELETE FROM \"tasks\";";
        String sqlLoadData = "INSERT INTO \"tasks\" (SELECT * FROM \"backup_tasks\");";
        String sqlDropBackupTable = "DROP TABLE \"backup_tasks\";";
        try {
            Statement statement = connection.createStatement();
            statement.executeUpdate(sqlClearTable);
            statement.executeUpdate(sqlLoadData);
            statement.executeUpdate(sqlDropBackupTable);
            connection.commit();
        } catch (Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @SneakyThrows
    public static void getTestData() {
        @NotNull final String userName = propertyService.getDatabaseUsername();
        @NotNull final String userPassword = propertyService.getDatabasePassword();
        @NotNull final String url = propertyService.getDatabaseConnectionString();
        @NotNull final Connection connection = DriverManager.getConnection(url, userName, userPassword);
        connection.setAutoCommit(false);
        String sqlGetTestUserIdData = "SELECT id FROM \"users\" WHERE \"login\" ='test';";
        try {
            Statement statement = connection.createStatement();
            @NotNull final ResultSet userSet = statement.executeQuery(sqlGetTestUserIdData);
            if (userSet.next()) {
                userId = userSet.getString(1);
            }
            String sqlGetTestProjectIdData = "SELECT id FROM \"projects\" WHERE \"user_id\" = '" + userId + "';";
            @NotNull final ResultSet projectSet = statement.executeQuery(sqlGetTestProjectIdData);
            if (projectSet.next()) {
                projectId = projectSet.getString(1);
            } else {
                @NotNull final ProjectDTO project = createOneProject();
                projectService.add(project);
                projectId = project.getId();
            }
            connection.commit();
        } catch (Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @NotNull
    private static ProjectDTO createOneProject() {
        @NotNull final ProjectDTO project = new ProjectDTO();
        project.setId(projectId);
        project.setUserId(userId);
        project.setName("TEST");
        project.setDescription("TEST");
        project.setStatus(Status.IN_PROGRESS);
        return project;
    }

    @Before
    public void setup() {
        task = createOneTask();
        tasks = createManyTasks();
    }

    @After
    public void cleanup() {
        taskService.clear();
    }

    @Test
    public void add() {
        taskService.add(task);
        Assert.assertEquals(1, taskService.getSize());
        taskService.add(tasks);
        Assert.assertEquals(REPOSITORY_SIZE + 1, taskService.getSize());
    }

    @Test
    public void set() {
        taskService.set(tasks);
        Assert.assertEquals(REPOSITORY_SIZE, taskService.getSize());
    }

    @Test
    public void existById() {
        taskService.add(task);
        Assert.assertFalse(taskService.existsById(NULL_TASK_ID));
        Assert.assertFalse(taskService.existsById(EMPTY_TASK_ID));
        Assert.assertTrue(taskService.existsById(taskId));
    }

    @Test
    public void findAll() {
        taskService.add(tasks);
        Assert.assertEquals(tasks.size(), taskService.findAll().size());
    }

    @Test
    public void findById() {
        taskService.add(task);
        taskService.add(tasks);
        Assert.assertThrows(TaskIdEmptyException.class, () -> taskService.findById(NULL_TASK_ID));
        Assert.assertThrows(TaskIdEmptyException.class, () -> taskService.findById(EMPTY_TASK_ID));
        Assert.assertNull(taskService.findById(UUID.randomUUID().toString()));
        @Nullable final TaskDTO repositoryTask = taskService.findById(taskId);
        Assert.assertNotNull(repositoryTask);
        Assert.assertEquals(task.getId(), repositoryTask.getId());
    }

    @Test
    public void remove() {
        taskService.add(task);
        Assert.assertThrows(TaskNotFoundException.class, () -> taskService.remove(NULL_TASK));
        taskService.remove(task);
        @Nullable final TaskDTO repositoryTask = taskService.findById(taskId);
        Assert.assertNull(repositoryTask);
    }

    @Test
    public void removeById() {
        taskService.add(task);
        taskService.add(tasks);
        Assert.assertThrows(TaskIdEmptyException.class, () -> taskService.removeById(NULL_TASK_ID));
        Assert.assertThrows(TaskIdEmptyException.class, () -> taskService.removeById(EMPTY_TASK_ID));
        taskService.removeById(taskId);
        @Nullable final TaskDTO repositoryTask = taskService.findById(taskId);
        Assert.assertNull(repositoryTask);
    }

    @Test
    public void clear() {
        taskService.add(tasks);
        Assert.assertEquals(REPOSITORY_SIZE, taskService.getSize());
        taskService.clear();
        Assert.assertEquals(EMPTY_REPOSITORY_SIZE, taskService.getSize());
    }

    @Test
    public void getSize() {
        taskService.add(tasks);
        Assert.assertEquals(REPOSITORY_SIZE, taskService.getSize());
    }

    @Test
    public void addWithUserId() {
        taskService.add(userId, task);
        Assert.assertEquals(1, taskService.getSize());
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.add(NULL_USER_ID, task));
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.add(EMPTY_USER_ID, task));
        Assert.assertThrows(TaskNotFoundException.class, () -> taskService.add(userId, NULL_TASK));
    }

    @Test
    public void existByIdWithUserId() {
        taskService.add(userId, task);
        Assert.assertTrue(taskService.existsById(userId, taskId));
        Assert.assertFalse(taskService.existsById(NULL_USER_ID, taskId));
        Assert.assertFalse(taskService.existsById(EMPTY_USER_ID, taskId));
        Assert.assertFalse(taskService.existsById(userId, NULL_TASK_ID));
        Assert.assertFalse(taskService.existsById(userId, EMPTY_TASK_ID));
        Assert.assertTrue(taskService.existsById(userId, taskId));
    }

    @Test
    public void findAllWithUserId() {
        for (TaskDTO task : tasks)
            taskService.add(userId, task);
        taskService.add(userId, task);
        Assert.assertEquals(REPOSITORY_SIZE + 1, taskService.findAll(userId).size());
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.findAll(NULL_USER_ID));
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.findAll(EMPTY_USER_ID));
    }

    @Test
    public void findByIdWithUserId() {
        for (TaskDTO task : tasks)
            taskService.add(userId, task);
        taskService.add(userId, task);
        @Nullable final TaskDTO repositoryTask = taskService.findById(userId, taskId);
        Assert.assertNotNull(repositoryTask);
        Assert.assertEquals(task.getId(), repositoryTask.getId());
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.findById(NULL_USER_ID, taskId));
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.findById(EMPTY_USER_ID, taskId));
        Assert.assertThrows(TaskIdEmptyException.class, () -> taskService.findById(userId, NULL_TASK_ID));
        Assert.assertThrows(TaskIdEmptyException.class, () -> taskService.findById(userId, EMPTY_TASK_ID));
    }

    @Test
    public void removeWithUserId() {
        taskService.add(userId, task);
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.remove(NULL_USER_ID, task));
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.remove(EMPTY_USER_ID, task));
        Assert.assertThrows(TaskNotFoundException.class, () -> taskService.remove(userId, NULL_TASK));
        taskService.remove(userId, task);
        @Nullable final TaskDTO repositoryTask = taskService.findById(userId, taskId);
        Assert.assertNull(repositoryTask);
    }

    @Test
    public void removeByIdWithUserId() {
        taskService.add(userId, task);
        for (TaskDTO task : tasks)
            taskService.add(userId, task);
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.removeById(NULL_USER_ID, taskId));
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.removeById(EMPTY_USER_ID, taskId));
        Assert.assertThrows(TaskIdEmptyException.class, () -> taskService.removeById(userId, NULL_TASK_ID));
        Assert.assertThrows(TaskIdEmptyException.class, () -> taskService.removeById(userId, EMPTY_TASK_ID));
        taskService.removeById(userId, task.getId());
        @Nullable final TaskDTO repositoryTask = taskService.findById(userId, taskId);
        Assert.assertNull(repositoryTask);
    }

    @Test
    public void clearWithUserId() {
        taskService.add(userId, task);
        Assert.assertEquals(1, taskService.getSize(userId));
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.clear(NULL_USER_ID));
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.clear(EMPTY_USER_ID));
        taskService.clear(userId);
        Assert.assertEquals(EMPTY_REPOSITORY_SIZE, taskService.getSize());
    }

    @Test
    public void getSizeWithUserId() {
        taskService.add(userId, task);
        Assert.assertEquals(1, taskService.getSize(userId));
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.getSize(NULL_USER_ID));
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.getSize(EMPTY_USER_ID));
        taskService.clear(userId);
        Assert.assertEquals(EMPTY_REPOSITORY_SIZE, taskService.getSize());
    }

    @Test
    public void create() {
        taskService.add(userId, task);
        Assert.assertEquals(1, taskService.getSize(userId));
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.create(NULL_USER_ID, TASK_NAME_PREFIX));
        Assert.assertThrows(NameEmptyException.class, () -> taskService.create(userId, NULL_TASK_NAME));
        Assert.assertThrows(
                DescriptionEmptyException.class,
                () -> taskService.create(userId, TASK_NAME_PREFIX, NULL_TASK_DESCRIPTION)
        );
    }

    @Test
    public void updateById() {
        taskService.add(userId, task);
        Assert.assertEquals(1, taskService.getSize(userId));
        Assert.assertThrows(
                UserIdEmptyException.class,
                () -> taskService.updateById(NULL_USER_ID, taskId, TASK_NAME_PREFIX, TASK_DESCRIPTION_PREFIX)
        );
        Assert.assertThrows(
                TaskIdEmptyException.class,
                () -> taskService.updateById(userId, NULL_TASK_ID, TASK_NAME_PREFIX, TASK_DESCRIPTION_PREFIX)
        );
        Assert.assertThrows(
                NameEmptyException.class,
                () -> taskService.updateById(userId, taskId, NULL_TASK_NAME, TASK_DESCRIPTION_PREFIX)
        );
        Assert.assertThrows(
                DescriptionEmptyException.class,
                () -> taskService.updateById(userId, taskId, TASK_NAME_PREFIX, NULL_TASK_DESCRIPTION)
        );
    }

    @Test
    public void changeStatusById() {
        taskService.add(userId, task);
        Assert.assertEquals(1, taskService.getSize(userId));
        Assert.assertThrows(
                UserIdEmptyException.class,
                () -> taskService.changeTaskStatusById(NULL_USER_ID, taskId, Status.NOT_STARTED)
        );
        Assert.assertThrows(
                TaskIdEmptyException.class,
                () -> taskService.changeTaskStatusById(userId, NULL_TASK_ID, Status.NOT_STARTED)
        );
        Assert.assertThrows(
                StatusNotFoundException.class,
                () -> taskService.changeTaskStatusById(userId, taskId, NULL_STATUS)
        );
    }

    @Test
    public void findAllByProjectId() {
        for (TaskDTO task : tasks)
            taskService.add(userId, task);
        Assert.assertThrows(UserNotFoundException.class, () -> taskService.findAllByProjectId(NULL_USER_ID, taskId));
        Assert.assertEquals(REPOSITORY_SIZE, taskService.findAllByProjectId(userId, projectId).size());
    }

    @NotNull
    private TaskDTO createOneTask() {
        @NotNull final TaskDTO task = new TaskDTO();
        task.setId(taskId);
        task.setUserId(userId);
        task.setName(TASK_NAME_PREFIX);
        task.setDescription(TASK_DESCRIPTION_PREFIX);
        task.setStatus(Status.IN_PROGRESS);
        task.setProjectId(projectId);
        return task;
    }

    @NotNull
    private List<TaskDTO> createManyTasks() {
        @NotNull final List<TaskDTO> tasks = new ArrayList<>();
        for (int i = 0; i < REPOSITORY_SIZE; i++) {
            TaskDTO task = new TaskDTO();
            task.setName(TASK_NAME_PREFIX + i);
            task.setDescription(TASK_DESCRIPTION_PREFIX + i);
            task.setUserId(userId);
            task.setProjectId(projectId);
            tasks.add(task);
        }
        return tasks;
    }

}
