package ru.t1consulting.nkolesnik.tm.service.model;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.t1consulting.nkolesnik.tm.api.service.IConnectionService;
import ru.t1consulting.nkolesnik.tm.api.service.IPropertyService;
import ru.t1consulting.nkolesnik.tm.api.service.model.IProjectService;
import ru.t1consulting.nkolesnik.tm.api.service.model.IUserService;
import ru.t1consulting.nkolesnik.tm.enumerated.Role;
import ru.t1consulting.nkolesnik.tm.enumerated.Status;
import ru.t1consulting.nkolesnik.tm.exception.entity.ProjectNotFoundException;
import ru.t1consulting.nkolesnik.tm.exception.entity.StatusNotFoundException;
import ru.t1consulting.nkolesnik.tm.exception.field.DescriptionEmptyException;
import ru.t1consulting.nkolesnik.tm.exception.field.NameEmptyException;
import ru.t1consulting.nkolesnik.tm.exception.field.ProjectIdEmptyException;
import ru.t1consulting.nkolesnik.tm.exception.field.UserIdEmptyException;
import ru.t1consulting.nkolesnik.tm.marker.DataCategory;
import ru.t1consulting.nkolesnik.tm.model.Project;
import ru.t1consulting.nkolesnik.tm.model.User;
import ru.t1consulting.nkolesnik.tm.service.ConnectionService;
import ru.t1consulting.nkolesnik.tm.service.PropertyService;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;
import java.util.List;
import java.util.UUID;
import java.util.Vector;

@Category(DataCategory.class)
public class ProjectServiceTest {

    @NotNull
    private static final String PROJECT_NAME_PREFIX = "TEST_PROJECT_NAME";

    @NotNull
    private static final String PROJECT_DESCRIPTION_PREFIX = "TEST_PROJECT_DESCRIPTION";

    @NotNull
    private static final String USER_LOGIN = "test";

    @Nullable
    private static final String NULL_PROJECT_NAME = null;

    @Nullable
    private static final String NULL_PROJECT_DESCRIPTION = null;

    @Nullable
    private static final Status NULL_STATUS = null;

    @Nullable
    private static final String NULL_PROJECT_ID = null;

    @Nullable
    private static final String NULL_USER_ID = null;

    @NotNull
    private static final String EMPTY_PROJECT_ID = "";

    @NotNull
    private static final String EMPTY_USER_ID = "";

    @Nullable
    private static final Project NULL_PROJECT = null;

    private static final long REPOSITORY_SIZE = 10L;

    private static final long EMPTY_REPOSITORY_SIZE = 0L;

    @NotNull
    private static final IPropertyService propertyService = new PropertyService();
    @NotNull
    private static final IConnectionService connectionService = new ConnectionService(propertyService);
    @NotNull
    private static final IProjectService projectService = new ProjectService(connectionService);
    @NotNull
    private static final IUserService userService = new UserService(connectionService, propertyService);
    @NotNull
    private static String userId = "";
    @NotNull
    private static String projectId = "";
    @Nullable
    private static User user;
    @Nullable
    private static Project project;
    @NotNull
    private List<Project> projects;

    @BeforeClass
    public static void prepareTestEnvironment() {
        getTestData();
        saveDbBackup();
    }

    @AfterClass
    public static void restoreProdEnvironment() {
        loadDbBackup();
    }

    @SneakyThrows
    public static void saveDbBackup() {
        @NotNull final String userName = propertyService.getDatabaseUsername();
        @NotNull final String userPassword = propertyService.getDatabasePassword();
        @NotNull final String url = propertyService.getDatabaseConnectionString();
        @NotNull final Connection connection = DriverManager.getConnection(url, userName, userPassword);
        connection.setAutoCommit(false);
        String sqlSaveProjectData = "SELECT * INTO \"backup_projects\" FROM \"projects\";";
        String sqlClearProjectTable = "DELETE FROM \"projects\";";
        String sqlSaveTaskData = "SELECT * INTO \"backup_tasks\" FROM \"tasks\";";
        String sqlClearTaskTable = "DELETE FROM \"tasks\";";
        try {
            Statement statement = connection.createStatement();
            statement.executeUpdate(sqlSaveTaskData);
            statement.executeUpdate(sqlSaveProjectData);
            statement.executeUpdate(sqlClearTaskTable);
            statement.executeUpdate(sqlClearProjectTable);
            connection.commit();
        } catch (Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @SneakyThrows
    public static void loadDbBackup() {
        projectService.remove(project);
        @NotNull final String userName = propertyService.getDatabaseUsername();
        @NotNull final String userPassword = propertyService.getDatabasePassword();
        @NotNull final String url = propertyService.getDatabaseConnectionString();
        @NotNull final Connection connection = DriverManager.getConnection(url, userName, userPassword);
        connection.setAutoCommit(false);
        @NotNull final String sqlClearProjectTable = "DELETE FROM \"projects\";";
        @NotNull final String sqlLoadProjectData = "INSERT INTO \"projects\" (SELECT * FROM \"backup_projects\");";
        @NotNull final String sqlDropBackupProjectTable = "DROP TABLE \"backup_projects\";";
        @NotNull final String sqlClearTaskTable = "DELETE FROM \"tasks\";";
        @NotNull final String sqlLoadTaskData = "INSERT INTO \"tasks\" (SELECT * FROM \"backup_tasks\");";
        @NotNull final String sqlDropBackupTaskTable = "DROP TABLE \"backup_tasks\";";
        try {
            Statement statement = connection.createStatement();
            statement.executeUpdate(sqlClearTaskTable);
            statement.executeUpdate(sqlClearProjectTable);
            statement.executeUpdate(sqlLoadProjectData);
            statement.executeUpdate(sqlLoadTaskData);
            statement.executeUpdate(sqlDropBackupTaskTable);
            statement.executeUpdate(sqlDropBackupProjectTable);
            connection.commit();
        } catch (Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @SneakyThrows
    public static void getTestData() {
        user = userService.findByLogin(USER_LOGIN);
        if (user == null) {
            user = userService.create("TEST TEST", "1234", Role.USUAL);
        }
        userId = user.getId();
        if (project == null) {
            project = projectService.create(user.getId(), "TEST PROJ");
            projectId = project.getId();
        }
    }

    @Before
    public void setup() {
        projects = createManyProjects();
    }

    @After
    public void cleanup() {
        projectService.clear();
    }

    @Test
    public void add() {
        projectService.add(userId, project);
        Assert.assertEquals(1, projectService.getSize());
        projectService.add(userId, projects);
        Assert.assertEquals(REPOSITORY_SIZE + 1, projectService.getSize());
    }

    @Test
    public void set() {
        projectService.set(projects);
        Assert.assertEquals(REPOSITORY_SIZE, projectService.getSize());
    }

    @Test
    public void existById() {
        projectService.add(project);
        Assert.assertFalse(projectService.existsById(NULL_PROJECT_ID));
        Assert.assertFalse(projectService.existsById(EMPTY_PROJECT_ID));
        Assert.assertTrue(projectService.existsById(projectId));
    }

    @Test
    public void findAll() {
        projectService.add(projects);
        Assert.assertEquals(projects.size(), projectService.findAll().size());
    }

    @Test
    public void findById() {
        projectService.add(project);
        projectService.add(projects);
        Assert.assertThrows(ProjectIdEmptyException.class, () -> projectService.findById(NULL_PROJECT_ID));
        Assert.assertThrows(ProjectIdEmptyException.class, () -> projectService.findById(EMPTY_PROJECT_ID));
        Assert.assertNull(projectService.findById(UUID.randomUUID().toString()));
        @Nullable final Project repositoryProject = projectService.findById(projectId);
        Assert.assertNotNull(repositoryProject);
        Assert.assertEquals(project.getId(), repositoryProject.getId());
    }

    @Test
    public void remove() {
        projectService.add(project);
        Assert.assertThrows(ProjectNotFoundException.class, () -> projectService.remove(NULL_PROJECT));
        projectService.remove(project);
        @Nullable final Project repositoryProject = projectService.findById(projectId);
        Assert.assertNull(repositoryProject);
    }

    @Test
    public void removeById() {
        projectService.add(project);
        projectService.add(projects);
        Assert.assertThrows(ProjectIdEmptyException.class, () -> projectService.removeById(NULL_PROJECT_ID));
        Assert.assertThrows(ProjectIdEmptyException.class, () -> projectService.removeById(EMPTY_PROJECT_ID));
        projectService.removeById(projectId);
        @Nullable final Project repositoryProject = projectService.findById(projectId);
        Assert.assertNull(repositoryProject);
    }

    @Test
    public void clear() {
        projectService.add(projects);
        Assert.assertEquals(REPOSITORY_SIZE, projectService.getSize());
        projectService.clear();
        Assert.assertEquals(EMPTY_REPOSITORY_SIZE, projectService.getSize());
    }

    @Test
    public void getSize() {
        projectService.add(projects);
        Assert.assertEquals(REPOSITORY_SIZE, projectService.getSize());
    }

    @Test
    public void addWithUserId() {
        projectService.add(userId, project);
        Assert.assertEquals(1, projectService.getSize());
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.add(NULL_USER_ID, project));
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.add(EMPTY_USER_ID, project));
        Assert.assertThrows(ProjectNotFoundException.class, () -> projectService.add(userId, NULL_PROJECT));
    }

    @Test
    public void existByIdWithUserId() {
        projectService.add(userId, project);
        Assert.assertTrue(projectService.existsById(userId, projectId));
        Assert.assertFalse(projectService.existsById(NULL_USER_ID, projectId));
        Assert.assertFalse(projectService.existsById(EMPTY_USER_ID, projectId));
        Assert.assertFalse(projectService.existsById(userId, NULL_PROJECT_ID));
        Assert.assertFalse(projectService.existsById(userId, EMPTY_PROJECT_ID));
        Assert.assertTrue(projectService.existsById(userId, projectId));
    }

    @Test
    public void findAllWithUserId() {
        projectService.add(userId, project);
        Assert.assertEquals(1, projectService.findAll(userId).size());
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.findAll(NULL_USER_ID));
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.findAll(EMPTY_USER_ID));
    }

    @Test
    public void findByIdWithUserId() {
        projectService.add(userId, project);
        @Nullable final Project repositoryProject = projectService.findById(userId, projectId);
        Assert.assertNotNull(repositoryProject);
        Assert.assertEquals(project.getId(), repositoryProject.getId());
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.findById(NULL_USER_ID, projectId));
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.findById(EMPTY_USER_ID, projectId));
        Assert.assertThrows(ProjectIdEmptyException.class, () -> projectService.findById(userId, NULL_PROJECT_ID));
        Assert.assertThrows(ProjectIdEmptyException.class, () -> projectService.findById(userId, EMPTY_PROJECT_ID));
    }

    @Test
    public void removeWithUserId() {
        projectService.add(userId, project);
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.remove(NULL_USER_ID, project));
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.remove(EMPTY_USER_ID, project));
        Assert.assertThrows(ProjectNotFoundException.class, () -> projectService.remove(userId, NULL_PROJECT));
        projectService.remove(userId, project);
        @Nullable final Project repositoryProject = projectService.findById(userId, projectId);
        Assert.assertNull(repositoryProject);
    }

    @Test
    public void removeByIdWithUserId() {
        projectService.add(userId, project);
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.removeById(NULL_USER_ID, projectId));
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.removeById(EMPTY_USER_ID, projectId));
        Assert.assertThrows(ProjectIdEmptyException.class, () -> projectService.removeById(userId, NULL_PROJECT_ID));
        Assert.assertThrows(ProjectIdEmptyException.class, () -> projectService.removeById(userId, EMPTY_PROJECT_ID));
        projectService.removeById(userId, projectId);
        @Nullable final Project repositoryProject = projectService.findById(userId, projectId);
        Assert.assertNull(repositoryProject);
    }

    @Test
    public void clearWithUserId() {
        projectService.add(userId, project);
        Assert.assertEquals(1, projectService.getSize(userId));
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.clear(NULL_USER_ID));
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.clear(EMPTY_USER_ID));
        projectService.clear(userId);
        Assert.assertEquals(EMPTY_REPOSITORY_SIZE, projectService.getSize());
    }

    @Test
    public void getSizeWithUserId() {
        projectService.add(userId, project);
        Assert.assertEquals(1, projectService.getSize(userId));
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.getSize(NULL_USER_ID));
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.getSize(EMPTY_USER_ID));
        projectService.clear(userId);
        Assert.assertEquals(EMPTY_REPOSITORY_SIZE, projectService.getSize());
    }

    @Test
    public void create() {
        projectService.add(userId, project);
        Assert.assertEquals(1, projectService.getSize(userId));
        Assert.assertThrows(
                UserIdEmptyException.class,
                () -> projectService.create(NULL_USER_ID, PROJECT_NAME_PREFIX)
        );
        Assert.assertThrows(NameEmptyException.class, () -> projectService.create(userId, NULL_PROJECT_NAME));
        Assert.assertThrows(
                DescriptionEmptyException.class,
                () -> projectService.create(userId, PROJECT_NAME_PREFIX, NULL_PROJECT_DESCRIPTION)
        );
    }

    @Test
    public void updateById() {
        projectService.add(userId, project);
        Assert.assertEquals(1, projectService.getSize(userId));
        Assert.assertThrows(
                UserIdEmptyException.class,
                () -> projectService.updateById(
                        NULL_USER_ID, projectId,
                        PROJECT_NAME_PREFIX,
                        PROJECT_DESCRIPTION_PREFIX
                )
        );
        Assert.assertThrows(
                ProjectIdEmptyException.class,
                () -> projectService.updateById(
                        userId, NULL_PROJECT_ID,
                        PROJECT_NAME_PREFIX,
                        PROJECT_DESCRIPTION_PREFIX
                )
        );
        Assert.assertThrows(
                NameEmptyException.class,
                () -> projectService.updateById(userId, projectId, NULL_PROJECT_NAME, PROJECT_DESCRIPTION_PREFIX)
        );
        Assert.assertThrows(
                DescriptionEmptyException.class,
                () -> projectService.updateById(userId, projectId, PROJECT_NAME_PREFIX, NULL_PROJECT_DESCRIPTION)
        );
    }

    @Test
    public void changeStatusById() {
        projectService.add(userId, project);
        Assert.assertEquals(1, projectService.getSize(userId));
        Assert.assertThrows(
                UserIdEmptyException.class,
                () -> projectService.changeProjectStatusById(NULL_USER_ID, projectId, Status.NOT_STARTED)
        );
        Assert.assertThrows(
                ProjectIdEmptyException.class,
                () -> projectService.changeProjectStatusById(userId, NULL_PROJECT_ID, Status.NOT_STARTED)
        );
        Assert.assertThrows(
                StatusNotFoundException.class,
                () -> projectService.changeProjectStatusById(userId, projectId, NULL_STATUS)
        );
    }

    @NotNull
    private Project createOneProject() {
        @NotNull final Project project = new Project();
        project.setId(projectId);
        project.setName(PROJECT_NAME_PREFIX);
        project.setDescription(PROJECT_DESCRIPTION_PREFIX);
        project.setStatus(Status.IN_PROGRESS);
        return project;
    }

    @NotNull
    private List<Project> createManyProjects() {
        @NotNull final List<Project> projects = new Vector<>();
        for (int i = 0; i < REPOSITORY_SIZE; i++) {
            Project project = new Project();
            project.setName(PROJECT_NAME_PREFIX + i);
            project.setDescription(PROJECT_DESCRIPTION_PREFIX + i);
            project.setUser(user);
            projects.add(project);
        }
        return projects;
    }

}
