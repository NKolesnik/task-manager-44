package ru.t1consulting.nkolesnik.tm.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1consulting.nkolesnik.tm.api.repository.dto.IProjectRepositoryDTO;
import ru.t1consulting.nkolesnik.tm.api.repository.dto.ITaskRepositoryDTO;
import ru.t1consulting.nkolesnik.tm.api.repository.dto.IUserRepositoryDTO;
import ru.t1consulting.nkolesnik.tm.api.service.IConnectionService;
import ru.t1consulting.nkolesnik.tm.api.service.IPropertyService;
import ru.t1consulting.nkolesnik.tm.api.service.dto.IUserDtoService;
import ru.t1consulting.nkolesnik.tm.dto.model.UserDTO;
import ru.t1consulting.nkolesnik.tm.enumerated.Role;
import ru.t1consulting.nkolesnik.tm.exception.entity.UserNotFoundException;
import ru.t1consulting.nkolesnik.tm.exception.field.*;
import ru.t1consulting.nkolesnik.tm.repository.dto.ProjectRepositoryDTO;
import ru.t1consulting.nkolesnik.tm.repository.dto.TaskRepositoryDTO;
import ru.t1consulting.nkolesnik.tm.repository.dto.UserRepositoryDTO;
import ru.t1consulting.nkolesnik.tm.util.HashUtil;

import javax.persistence.EntityManager;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public final class UserDtoService extends AbstractDtoService<UserDTO, IUserRepositoryDTO> implements IUserDtoService {

    @NotNull
    private final IPropertyService propertyService;

    public UserDtoService(@NotNull final IConnectionService connectionService,
                          @NotNull final IPropertyService propertyService
    ) {
        super(connectionService);
        this.propertyService = propertyService;
    }


    @Override
    public void add(@Nullable final UserDTO user) {
        if (user == null) throw new UserNotFoundException();
        @NotNull final EntityManager em = connectionService.getEntityManager();
        try {
            @NotNull final IUserRepositoryDTO repository = new UserRepositoryDTO(em);
            em.getTransaction().begin();
            repository.add(user);
            em.getTransaction().commit();
        } catch (Exception e) {
            em.getTransaction().rollback();
            throw e;
        } finally {
            em.close();
        }
    }

    @Override
    public void addALl(@Nullable final Collection<UserDTO> users) {
        if (users == null || users.isEmpty()) throw new UserNotFoundException();
        @NotNull final EntityManager em = connectionService.getEntityManager();
        try {
            @NotNull final IUserRepositoryDTO repository = new UserRepositoryDTO(em);
            em.getTransaction().begin();
            for (UserDTO user : users)
                repository.add(user);
            em.getTransaction().commit();
        } catch (Exception e) {
            em.getTransaction().rollback();
            throw e;
        } finally {
            em.close();
        }
    }

    @Override
    public void set(@Nullable final Collection<UserDTO> users) {
        if (users == null || users.isEmpty()) throw new UserNotFoundException();
        @NotNull final EntityManager em = connectionService.getEntityManager();
        try {
            @NotNull final IUserRepositoryDTO repository = new UserRepositoryDTO(em);
            em.getTransaction().begin();
            repository.clear();
            for (UserDTO user : users)
                repository.add(user);
            em.getTransaction().commit();
        } catch (Exception e) {
            em.getTransaction().rollback();
            throw e;
        } finally {
            em.close();
        }
    }

    @Override
    public long getSize() {
        @NotNull final EntityManager em = connectionService.getEntityManager();
        try {
            @NotNull final IUserRepositoryDTO repository = new UserRepositoryDTO(em);
            return repository.getSize();
        } finally {
            em.close();
        }
    }

    @NotNull
    @Override
    public List<UserDTO> findAll() {
        @Nullable final List<UserDTO> users;
        @NotNull final EntityManager em = connectionService.getEntityManager();
        try {
            @NotNull final IUserRepositoryDTO repository = new UserRepositoryDTO(em);
            users = repository.findAll();
            if (users.isEmpty()) return Collections.emptyList();
            return users;
        } finally {
            em.close();
        }
    }

    @Nullable
    @Override
    public UserDTO findById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new TaskIdEmptyException();
        @NotNull final EntityManager em = connectionService.getEntityManager();
        try {
            @NotNull final IUserRepositoryDTO repository = new UserRepositoryDTO(em);
            return repository.findById(id);
        } finally {
            em.close();
        }
    }

    @Override
    public void clear() {
        @NotNull final EntityManager em = connectionService.getEntityManager();
        try {
            @NotNull final IUserRepositoryDTO repository = new UserRepositoryDTO(em);
            em.getTransaction().begin();
            repository.clear();
            em.getTransaction().commit();
        } catch (Exception e) {
            em.getTransaction().rollback();
            throw e;
        } finally {
            em.close();
        }
    }

    @Override
    public void removeById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new UserIdEmptyException();
        @NotNull final EntityManager em = connectionService.getEntityManager();
        try {
            @NotNull final IUserRepositoryDTO repository = new UserRepositoryDTO(em);
            em.getTransaction().begin();
            repository.removeById(id);
            em.getTransaction().commit();
        } catch (Exception e) {
            em.getTransaction().rollback();
            throw e;
        } finally {
            em.close();
        }
    }

    @NotNull
    @Override
    public UserDTO create(@Nullable final String login, @Nullable final String password) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExist(login)) throw new LoginAlreadyExistException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @NotNull final UserDTO user = new UserDTO();
        @NotNull final EntityManager em = connectionService.getEntityManager();
        try {
            @NotNull final IUserRepositoryDTO repository = new UserRepositoryDTO(em);
            em.getTransaction().begin();
            user.setLogin(login);
            user.setRole(Role.USUAL);
            @NotNull final String secret = propertyService.getPasswordSecret();
            @NotNull final Integer iteration = propertyService.getPasswordIteration();
            user.setPasswordHash(HashUtil.salt(password, secret, iteration));
            repository.add(user);
            em.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            em.getTransaction().rollback();
            throw e;
        } finally {
            em.close();
        }
        return user;
    }

    @NotNull
    @Override
    public UserDTO create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final String email
    ) {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        if (isEmailExist(email)) throw new EmailAlreadyExistException();
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExist(login)) throw new LoginAlreadyExistException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @NotNull final EntityManager em = connectionService.getEntityManager();
        @NotNull final UserDTO user = new UserDTO();
        try {
            @NotNull final IUserRepositoryDTO repository = new UserRepositoryDTO(em);
            em.getTransaction().begin();
            user.setLogin(login);
            user.setRole(Role.USUAL);
            @NotNull final String secret = propertyService.getPasswordSecret();
            @NotNull final Integer iteration = propertyService.getPasswordIteration();
            user.setPasswordHash(HashUtil.salt(password, secret, iteration));
            user.setEmail(email);
            repository.add(user);
            em.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            em.getTransaction().rollback();
            throw e;
        } finally {
            em.close();
        }
        return user;
    }

    @NotNull
    @Override
    public UserDTO create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final Role role
    ) {
        if (role == null) throw new RoleIsEmptyException();
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExist(login)) throw new LoginAlreadyExistException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @NotNull final UserDTO user = new UserDTO();
        @NotNull final EntityManager em = connectionService.getEntityManager();
        try {
            @NotNull final IUserRepositoryDTO repository = new UserRepositoryDTO(em);
            em.getTransaction().begin();
            user.setLogin(login);
            user.setRole(Role.USUAL);
            @NotNull final String secret = propertyService.getPasswordSecret();
            @NotNull final Integer iteration = propertyService.getPasswordIteration();
            user.setPasswordHash(HashUtil.salt(password, secret, iteration));
            user.setRole(role);
            repository.add(user);
            em.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            em.getTransaction().rollback();
            throw e;
        } finally {
            em.close();
        }
        return user;
    }

    @NotNull
    @Override
    public UserDTO create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final String email,
            @Nullable final Role role
    ) {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        if (isEmailExist(email)) throw new EmailAlreadyExistException();
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExist(login)) throw new LoginAlreadyExistException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        if (role == null) throw new RoleIsEmptyException();
        @NotNull final UserDTO user = new UserDTO();
        @NotNull final EntityManager em = connectionService.getEntityManager();
        try {
            @NotNull final IUserRepositoryDTO repository = new UserRepositoryDTO(em);
            em.getTransaction().begin();
            user.setLogin(login);
            user.setRole(Role.USUAL);
            @NotNull final String secret = propertyService.getPasswordSecret();
            @NotNull final Integer iteration = propertyService.getPasswordIteration();
            user.setPasswordHash(HashUtil.salt(password, secret, iteration));
            user.setEmail(email);
            user.setRole(role);
            repository.add(user);
            em.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            em.getTransaction().rollback();
            throw e;
        } finally {
            em.close();
        }
        return user;
    }

    @Nullable
    @Override
    public UserDTO findByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @NotNull final EntityManager em = connectionService.getEntityManager();
        try {
            @NotNull final IUserRepositoryDTO repository = new UserRepositoryDTO(em);
            return repository.findByLogin(login);
        } finally {
            em.close();
        }
    }

    @Nullable
    @Override
    public UserDTO findByEmail(@Nullable final String email) {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        @NotNull final EntityManager em = connectionService.getEntityManager();
        try {
            @NotNull final IUserRepositoryDTO repository = new UserRepositoryDTO(em);
            return repository.findByEmail(email);
        } finally {
            em.close();
        }
    }

    @Override
    public void setPassword(@Nullable final String id, @Nullable final String password) {
        if (id == null || id.isEmpty()) throw new UserIdEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @NotNull final EntityManager em = connectionService.getEntityManager();
        try {
            @NotNull final IUserRepositoryDTO repository = new UserRepositoryDTO(em);
            em.getTransaction().begin();
            @Nullable final UserDTO user = findById(id);
            if (user == null) throw new UserNotFoundException();
            repository.setPassword(user, password);
            em.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            em.getTransaction().rollback();
            throw e;
        } finally {
            em.close();
        }
    }

    @NotNull
    @Override
    public UserDTO updateUser(
            @Nullable final String id,
            @Nullable final String firstName,
            @Nullable final String middleName,
            @Nullable final String lastName) {
        if (id == null || id.isEmpty()) throw new UserIdEmptyException();
        @NotNull final EntityManager em = connectionService.getEntityManager();
        @Nullable final UserDTO result;
        try {
            @NotNull final IUserRepositoryDTO repository = new UserRepositoryDTO(em);
            em.getTransaction().begin();
            result = repository.findById(id);
            if (result == null) throw new UserNotFoundException();
            repository.updateUser(id, firstName, middleName, lastName);
            em.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            em.getTransaction().rollback();
            throw e;
        } finally {
            em.close();
        }
        return result;
    }

    @Override
    public void update(@Nullable final UserDTO user) {
        if (user == null) throw new UserNotFoundException();
        @NotNull final EntityManager em = connectionService.getEntityManager();
        try {
            @NotNull final IUserRepositoryDTO repository = new UserRepositoryDTO(em);
            em.getTransaction().begin();
            repository.update(user);
            em.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            em.getTransaction().rollback();
            throw e;
        } finally {
            em.close();
        }
    }

    @Override
    public void remove(@Nullable final UserDTO user) {
        if (user == null) throw new UserNotFoundException();
        @NotNull final EntityManager em = connectionService.getEntityManager();
        @Nullable final UserDTO result;
        try {
            @NotNull final IUserRepositoryDTO repository = new UserRepositoryDTO(em);
            em.getTransaction().begin();
            @NotNull final IProjectRepositoryDTO projectRepository = new ProjectRepositoryDTO(em);
            @NotNull final ITaskRepositoryDTO taskRepository = new TaskRepositoryDTO(em);
            result = repository.findById(user.getId());
            if (result == null) throw new UserNotFoundException();
            @NotNull final String userId = result.getId();
            taskRepository.clear(userId);
            projectRepository.clear(userId);
            repository.remove(result);
            em.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            em.getTransaction().rollback();
            throw e;
        } finally {
            em.close();
        }
    }

    @Override
    public void removeByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @NotNull final EntityManager em = connectionService.getEntityManager();
        try {
            @NotNull final IUserRepositoryDTO repository = new UserRepositoryDTO(em);
            em.getTransaction().begin();
            repository.removeByLogin(login);
            em.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            em.getTransaction().rollback();
            throw e;
        } finally {
            em.close();
        }
    }

    @NotNull
    @Override
    public Boolean isLoginExist(@Nullable final String login) {
        if (login == null || login.isEmpty()) return false;
        @NotNull final EntityManager em = connectionService.getEntityManager();
        try {
            @NotNull final IUserRepositoryDTO repository = new UserRepositoryDTO(em);
            return repository.isLoginExist(login);
        } finally {
            em.close();
        }
    }

    @NotNull
    @Override
    public Boolean isEmailExist(@Nullable final String email) {
        if (email == null || email.isEmpty()) return false;
        @NotNull final EntityManager em = connectionService.getEntityManager();
        try {
            @NotNull final IUserRepositoryDTO repository = new UserRepositoryDTO(em);
            return repository.isEmailExist(email);
        } finally {
            em.close();
        }
    }

    @Override
    public void lockUserByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @NotNull final EntityManager em = connectionService.getEntityManager();
        try {
            @NotNull final IUserRepositoryDTO repository = new UserRepositoryDTO(em);
            em.getTransaction().begin();
            @Nullable final UserDTO repositoryUser = repository.findByLogin(login);
            if (repositoryUser == null) throw new UserNotFoundException();
            repository.lockUserByLogin(login);
            em.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            em.getTransaction().rollback();
            throw e;
        } finally {
            em.close();
        }
    }

    @Override
    public void unlockUserByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @NotNull final EntityManager em = connectionService.getEntityManager();
        try {
            @NotNull final IUserRepositoryDTO repository = new UserRepositoryDTO(em);
            em.getTransaction().begin();
            @Nullable final UserDTO repositoryUser = repository.findByLogin(login);
            if (repositoryUser == null) throw new UserNotFoundException();
            repository.unlockUserByLogin(login);
            em.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            em.getTransaction().rollback();
            throw e;
        } finally {
            em.close();
        }
    }

}
