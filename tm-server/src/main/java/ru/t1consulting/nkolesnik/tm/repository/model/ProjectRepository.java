package ru.t1consulting.nkolesnik.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1consulting.nkolesnik.tm.api.repository.model.IProjectRepository;
import ru.t1consulting.nkolesnik.tm.enumerated.Sort;
import ru.t1consulting.nkolesnik.tm.model.Project;

import javax.persistence.EntityManager;
import java.util.Comparator;
import java.util.List;

public final class ProjectRepository extends AbstractUserOwnedRepository<Project> implements IProjectRepository {

    public ProjectRepository(@NotNull EntityManager entityManager) {
        super(entityManager);
    }

    @Nullable
    @Override
    public Project findById(@Nullable final String id) {
        return entityManager.find(Project.class, id);
    }

    @Nullable
    @Override
    public Project findById(@Nullable final String userId, @Nullable final String id) {
        return entityManager.
                createQuery("SELECT p FROM Project p WHERE p.id = :id AND p.user.id = :userId", Project.class).
                setParameter("id", id).
                setParameter("userId", userId).
                setMaxResults(1).getResultStream().findFirst().orElse(null);
    }

    @NotNull
    @Override
    public List<Project> findAll() {
        return entityManager.createQuery("FROM Project", Project.class).getResultList();
    }

    @NotNull
    @Override
    public List<Project> findAll(@Nullable final String userId) {
        return entityManager.
                createQuery("SELECT p FROM Project p WHERE p.user.id = :userId", Project.class).
                setParameter("userId", userId).
                getResultList();
    }

    @NotNull
    public List<Project> findAll(@Nullable final Sort sort) {
        if (sort == null) return findAll();
        return entityManager.
                createQuery("SELECT p FROM Project p ORDER BY :sort DESC", Project.class).
                setParameter("sort", getSortColumnName(sort.getComparator())).
                getResultList();
    }

    @NotNull
    public List<Project> findAll(@Nullable final Comparator comparator) {
        if (comparator == null) return findAll();
        return entityManager.
                createQuery("SELECT p FROM Project p ORDER BY :sort DESC", Project.class).
                setParameter("sort", getSortColumnName(comparator)).
                getResultList();
    }

    @NotNull
    public List<Project> findAll(@Nullable final String userId, @Nullable final Sort sort) {
        if (sort == null) return findAll();
        return entityManager.
                createQuery("SELECT p FROM Project p WHERE p.user.id = :userId ORDER BY :sort DESC", Project.class).
                setParameter("userId", userId).
                setParameter("sort", getSortColumnName(sort.getComparator())).
                getResultList();
    }

    @NotNull
    public List<Project> findAll(@Nullable final String userId, @Nullable final Comparator comparator) {
        if (comparator == null) return findAll();
        return entityManager.
                createQuery("SELECT p FROM Project p WHERE p.user.id = :userId ORDER BY :sort DESC", Project.class).
                setParameter("userId", userId).
                setParameter("sort", getSortColumnName(comparator)).
                getResultList();
    }

    @Override
    public boolean existsById(@Nullable final String id) {
        return entityManager.
                createQuery("SELECT COUNT(1) = 1 FROM Project p WHERE p.id = :id", Boolean.class).
                setParameter("id", id).
                getSingleResult();
    }

    @Override
    public boolean existsById(@Nullable final String userId, @Nullable final String id) {
        return entityManager.
                createQuery(
                        "SELECT COUNT(1) = 1 FROM Project p WHERE p.id = :id AND p.user.id= :userId", Boolean.class
                ).
                setParameter("userId", userId).
                setParameter("id", id).
                getSingleResult();
    }

    @Override
    public long getSize() {
        return entityManager.createQuery("SELECT COUNT(p) FROM Project p", Long.class).getSingleResult();
    }

    @Override
    public long getSize(@Nullable final String userId) {
        return entityManager.
                createQuery("SELECT COUNT(p) FROM Project p WHERE p.user.id = :userId", Long.class).
                setParameter("userId", userId).
                getSingleResult();
    }

    @Override
    public void remove(@Nullable final Project project) {
        entityManager.remove(project);
    }

    @Override
    public void remove(@Nullable final String userId, @Nullable final Project project) {
        if (project == null) return;
        @Nullable final Project repositoryProject = this.findById(userId, project.getId());
        if (repositoryProject == null) return;
        entityManager.remove(repositoryProject);
    }

    @Override
    public void removeById(@Nullable final String id) {
        @Nullable final Project project = findById(id);
        if (project == null) return;
        entityManager.remove(project);
    }

    @Override
    public void removeById(@Nullable final String userId, @Nullable final String id) {
        @Nullable final Project repositoryProject = this.findById(userId, id);
        if (repositoryProject == null) return;
        entityManager.remove(repositoryProject);
    }

    @Override
    public void clear() {
        for (@NotNull final Project project : findAll()) {
            entityManager.remove(project);
        }
    }

    @Override
    public void clear(@Nullable final String userId) {
        for (@NotNull final Project project : findAll(userId)) {
            entityManager.remove(project);
        }
    }

}
