package ru.t1consulting.nkolesnik.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1consulting.nkolesnik.tm.api.repository.model.IUserRepository;
import ru.t1consulting.nkolesnik.tm.api.service.IPropertyService;
import ru.t1consulting.nkolesnik.tm.model.User;
import ru.t1consulting.nkolesnik.tm.service.PropertyService;
import ru.t1consulting.nkolesnik.tm.util.HashUtil;

import javax.persistence.EntityManager;
import java.util.List;

public final class UserRepository extends AbstractRepository<User> implements IUserRepository {

    private IPropertyService propertyService = new PropertyService();

    public UserRepository(@NotNull EntityManager entityManager) {
        super(entityManager);
    }

    @Nullable
    @Override
    public User findByLogin(@Nullable final String login) {
        return entityManager.
                createQuery("SELECT u FROM User u WHERE u.login = :login", User.class).
                setParameter("login", login).
                setMaxResults(1).getResultStream().findFirst().orElse(null);
    }

    @Nullable
    @Override
    public User findByEmail(@Nullable final String email) {
        return entityManager.
                createQuery("SELECT u FROM User u WHERE u.email = :email", User.class).
                setParameter("email", email).
                setMaxResults(1).getResultStream().findFirst().orElse(null);
    }

    @NotNull
    @Override
    public Boolean isLoginExist(@Nullable final String login) {
        return entityManager.
                createQuery("SELECT COUNT (1) = 1 FROM User u WHERE u.login = :login", Boolean.class).
                setParameter("login", login).
                getSingleResult();
    }

    @NotNull
    @Override
    public Boolean isEmailExist(@Nullable final String email) {
        return entityManager.
                createQuery("SELECT COUNT (1) = 1 FROM User u WHERE u.email = :email", Boolean.class).
                setParameter("email", email).
                getSingleResult();
    }

    @Override
    public void lockUserByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) return;
        @Nullable final User user = findByLogin(login);
        if (user == null) return;
        user.setLocked(true);
        entityManager.merge(user);
    }

    @Override
    public void unlockUserByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) return;
        @Nullable final User user = findByLogin(login);
        if (user == null) return;
        user.setLocked(false);
        entityManager.merge(user);
    }

    @Override
    public void updateUser(
            @Nullable final String id,
            @Nullable final String firstName,
            @Nullable final String middleName,
            @Nullable final String lastName) {
        if (id == null || id.isEmpty()) return;
        @Nullable final User user = findById(id);
        if (user == null) return;
        user.setFirstName(firstName);
        user.setMiddleName(middleName);
        user.setLastName(lastName);
        entityManager.merge(user);
    }

    @Override
    public void setPassword(@Nullable final User user, @Nullable final String password) {
        if (user == null) return;
        if (password == null || password.isEmpty()) return;
        @Nullable final User repositoryUser = findById(user.getId());
        if (repositoryUser == null) return;
        repositoryUser.setPasswordHash(
                HashUtil.salt(password, propertyService.getPasswordSecret(), propertyService.getPasswordIteration())
        );
        entityManager.merge(repositoryUser);
    }

    @Override
    public void removeByLogin(@Nullable final String login) {
        @Nullable final User user = this.findByLogin(login);
        if (user == null) return;
        entityManager.remove(user);
    }

    @Override
    public long getSize() {
        return entityManager.createQuery("SELECT COUNT(u) FROM User u", Long.class).getSingleResult();
    }

    @NotNull
    @Override
    public List<User> findAll() {
        return entityManager.createQuery("SELECT u FROM User u", User.class).getResultList();
    }

    @Nullable
    @Override
    public User findById(@Nullable final String id) {
        return entityManager.
                createQuery("SELECT u FROM User u WHERE u.id = :id", User.class).
                setParameter("id", id).
                getResultStream().findFirst().orElse(null);
    }

    @Override
    public boolean existsById(@Nullable final String id) {
        return entityManager.contains(id);
    }

    @Override
    public void clear() {
        for (@NotNull final User user : findAll()) {
            entityManager.remove(user);
        }
    }

    @Override
    public void remove(@Nullable final User user) {
        entityManager.remove(user);
    }

    @Override
    public void removeById(@Nullable final String id) {
        @Nullable final User user = this.findById(id);
        if (user == null) return;
        entityManager.remove(user);
    }

}
