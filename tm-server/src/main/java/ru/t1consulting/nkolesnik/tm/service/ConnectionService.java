package ru.t1consulting.nkolesnik.tm.service;

import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.jetbrains.annotations.NotNull;
import ru.t1consulting.nkolesnik.tm.api.service.IConnectionService;
import ru.t1consulting.nkolesnik.tm.api.service.IPropertyService;
import ru.t1consulting.nkolesnik.tm.dto.model.ProjectDTO;
import ru.t1consulting.nkolesnik.tm.dto.model.TaskDTO;
import ru.t1consulting.nkolesnik.tm.dto.model.UserDTO;
import ru.t1consulting.nkolesnik.tm.model.Project;
import ru.t1consulting.nkolesnik.tm.model.Task;
import ru.t1consulting.nkolesnik.tm.model.User;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import java.util.HashMap;
import java.util.Map;

public class ConnectionService implements IConnectionService {

    @NotNull
    private final IPropertyService propertyService;

    @NotNull
    private final EntityManagerFactory entityManagerFactory;

    public ConnectionService(@NotNull final IPropertyService propertyService) {
        this.propertyService = propertyService;
        this.entityManagerFactory = getEntityManagerFactory();
    }

    @NotNull
    @Override
    public EntityManagerFactory getEntityManagerFactory() {
        @NotNull final Map<String, String> properties = new HashMap<>();
        properties.put(org.hibernate.cfg.Environment.DRIVER, propertyService.getDatabaseDriver());
        properties.put(org.hibernate.cfg.Environment.URL, propertyService.getDatabaseConnectionString());
        properties.put(org.hibernate.cfg.Environment.USER, propertyService.getDatabaseUsername());
        properties.put(org.hibernate.cfg.Environment.PASS, propertyService.getDatabasePassword());
        properties.put(org.hibernate.cfg.Environment.DIALECT, propertyService.getDatabaseDialect());
        properties.put(org.hibernate.cfg.Environment.HBM2DDL_AUTO, propertyService.getDatabaseHbm2Ddl());
        properties.put(org.hibernate.cfg.Environment.SHOW_SQL, propertyService.getDatabaseShowSql());
        properties.put(org.hibernate.cfg.Environment.FORMAT_SQL, propertyService.getDatabaseFormatSql());
        @NotNull final StandardServiceRegistryBuilder registryBuilder = new StandardServiceRegistryBuilder();
        registryBuilder.applySettings(properties);
        @NotNull final StandardServiceRegistry registry = registryBuilder.build();
        @NotNull final MetadataSources metadataSources = new MetadataSources(registry);
        metadataSources.addAnnotatedClass(User.class);
        metadataSources.addAnnotatedClass(UserDTO.class);
        metadataSources.addAnnotatedClass(Project.class);
        metadataSources.addAnnotatedClass(ProjectDTO.class);
        metadataSources.addAnnotatedClass(Task.class);
        metadataSources.addAnnotatedClass(TaskDTO.class);
        @NotNull final Metadata metadata = metadataSources.getMetadataBuilder().build();
        return metadata.getSessionFactoryBuilder().build();

    }

    @NotNull
    @Override
    public EntityManager getEntityManager() {
        return entityManagerFactory.createEntityManager();
    }

}