package ru.t1consulting.nkolesnik.tm.dto.response.task;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import ru.t1consulting.nkolesnik.tm.dto.model.TaskDTO;

@NoArgsConstructor
public class TaskCompleteByIdResponse extends AbstractTaskResponse {

    public TaskCompleteByIdResponse(@Nullable TaskDTO task) {
        super(task);
    }

}
